import dateformat from 'dateformat';
import { HttpClient } from './rest/HttpClient';

export interface Services {
    httpClient: HttpClient;
    dateTimeService: DateTimeService;
}

export class DateTimeService {
    public getNow(): Date {
        return new Date();
    }

    /**
     * @return yyy-mm-dd HH:MM:ss
     */
    public getNowTimestamp(): string {
        return formatYmdHis(this.getNow());
    }
}

export function formatYmdHis(date: Date) {
    return dateformat(date, 'yyy-mm-dd HH:MM:ss');
}
