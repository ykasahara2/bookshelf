import * as React from 'react';
import { NextPage } from 'next';
import { Content, Container, Section } from 'bloomer';

import { Layout } from '../components/Layout';

const IndexPage: NextPage = () => {
    return (
        <Layout title="Home">
            <Section>
                <Container>
                    <Content>
                        <p>本棚管理サービス</p>
                    </Content>
                </Container>
            </Section>
        </Layout>
    );
};

export default IndexPage;
