import React, { useState, FormEvent } from 'react';
import { NextPage } from 'next';
import { Field, Label, Control, Section, Container, Box, Content, Button } from 'bloomer';
import { Layout } from '../components/Layout';

const SignupPage: NextPage = () => {
    const [nickname, setNickname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleFormSubmit = async (e: FormEvent) => {
        e.preventDefault();
    };

    return (
        <Layout title="Signup">
            <Section>
                <Container>
                    <Content>
                        <Box>
                            <form onSubmit={handleFormSubmit}>
                                <Field>
                                    <Label>Nickname</Label>
                                    <Control>
                                        <input
                                            className="input"
                                            type="text"
                                            value={nickname}
                                            onChange={e => {
                                                setNickname(e.target.value);
                                            }}
                                        />
                                    </Control>
                                </Field>

                                <Field>
                                    <Label>Email</Label>
                                    <Control>
                                        <input
                                            className="input"
                                            type="text"
                                            value={email}
                                            onChange={e => {
                                                setEmail(e.target.value);
                                            }}
                                        />
                                    </Control>
                                </Field>

                                <Field>
                                    <Label>Password</Label>
                                    <Control>
                                        <input
                                            className="input"
                                            type="password"
                                            value={password}
                                            onChange={e => {
                                                setPassword(e.target.value);
                                            }}
                                        />
                                    </Control>
                                </Field>

                                <Field className="buttons">
                                    <Button isColor="link" type="submit">
                                        Signup
                                    </Button>
                                </Field>
                            </form>
                        </Box>
                    </Content>
                </Container>
            </Section>
        </Layout>
    );
};

export default SignupPage;
