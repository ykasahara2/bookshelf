import 'bulma/bulma.sass';
import React from 'react';
import App, { AppContext } from 'next/app';
import { Provider } from 'react-redux';
import '../scss/main.scss';
import { store } from '../store';
import { httpClient } from '../rest/HttpClient';

httpClient.init();
export class MyApp extends App {
    static async getInitialProps({ Component, ctx }: AppContext) {
        const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
        return { pageProps };
    }

    render() {
        const { Component, pageProps } = this.props;
        return (
            <Provider store={store}>
                <Component {...pageProps} />
            </Provider>
        );
    }
}

export default MyApp;
