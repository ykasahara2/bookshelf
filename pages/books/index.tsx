import React, { useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Section, Container, Content } from 'bloomer';

import { BookList } from '../../components/BookList';
import { Layout } from '../../components/Layout';
import { Book, fetchBooks } from '../../modules/BookModule';
import { RootState } from '../../reducer';

interface Props {
    books: Book[];
}

const BooksPage: NextPage<Props> = () => {
    const dispatch = useDispatch();
    const books = useSelector((state: RootState) => state.book.books);

    useEffect(() => {
        dispatch(fetchBooks());
    }, [dispatch]);

    return (
        <Layout title="Books">
            <Section>
                <Container>
                    <Content>
                        <p>
                            <Link href="/books/edit">
                                <a>
                                    <Button isColor="link">+ Add Book</Button>
                                </a>
                            </Link>
                        </p>
                    </Content>
                    <BookList books={books} />
                </Container>
            </Section>
        </Layout>
    );
};

export default BooksPage;
