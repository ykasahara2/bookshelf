import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NextPage, NextPageContext } from 'next';
import Link from 'next/link';
import Router from 'next/router';
import { Container, Section, Button, Content, Field, Control, Label } from 'bloomer';
import { Layout } from '../../components/Layout';
import { fetchBooks, deleteBook } from '../../modules/BookModule';
import { BookDetail } from '../../components/BookDetail';
import { RootState } from '../../reducer';
import { modelingBooks } from '../../models/BookModels';
import { postBookReview, fetchBookReviews } from '../../modules/BookReviewModule';

type Props = {
    bookID: string;
};

const BookDetailPage: NextPage<Props> = ({ bookID }) => {
    const [reviewText, setReviewText] = useState('');
    const dispatch = useDispatch();
    const books = useSelector((state: RootState) => state.book.books);
    const bookReviews = useSelector((state: RootState) => state.bookReview.bookReviews);
    const bookModels = modelingBooks(books, bookReviews);
    const bookModel = bookModels.getByID(bookID);

    useEffect(() => {
        dispatch(fetchBooks());
        dispatch(fetchBookReviews());
    }, [dispatch]);

    if (bookModel.isEmpty()) {
        return (
            <Layout title="Detail">
                <Section>
                    <Container>
                        <p>Not Found</p>
                    </Container>
                </Section>
            </Layout>
        );
    }

    const book = bookModel.get();

    const handleBookRemove = async (bookID: string) => {
        if (!confirm('この本を本棚から削除します。よろしいですか？')) {
            return;
        }

        await dispatch(deleteBook(bookID));
        Router.replace({
            pathname: '/books',
        });
    };

    const handleFormSubmit = async (e: React.FormEvent) => {
        e.preventDefault();

        if (!reviewText) {
            return;
        }

        await dispatch(postBookReview('', bookID, 'guest', reviewText));
        setReviewText('');
    };

    return (
        <Layout title={`${book ? book.title : 'Detail'}`}>
            <Section>
                <Container>
                    <Content>
                        <p className="buttons">
                            <Link href={{ pathname: '/books/edit', query: { id: book.bookID } }}>
                                <a className="button is-link">Edit</a>
                            </Link>
                            <Button onClick={() => handleBookRemove(book.bookID)}>Delete</Button>
                        </p>
                    </Content>
                    <Content>
                        {book && (
                            <Content>
                                <BookDetail book={book} />
                            </Content>
                        )}
                    </Content>
                    <Content>
                        <Label>Reviews</Label>
                        {book.bookReviews.length === 0 ? (
                            <p>レビューはまだありません。</p>
                        ) : (
                            book.bookReviews.map(br => (
                                <ul key={br.reviewID} className="list">
                                    <li className="list-item">
                                        <p>
                                            <span>{br.name}</span>
                                            {` `}
                                            <time>{br.postDate}</time>
                                        </p>
                                        <div>{br.comment}</div>
                                    </li>
                                </ul>
                            ))
                        )}
                    </Content>
                    <Content>
                        <form onSubmit={e => handleFormSubmit(e)}>
                            <Field>
                                <Label>Post Review</Label>
                                <Control>
                                    <textarea
                                        className="textarea"
                                        value={reviewText}
                                        onChange={e => setReviewText(e.target.value)}
                                    ></textarea>
                                </Control>
                            </Field>
                            <Field>
                                <Control>
                                    <Button isColor="link" type="submit">
                                        Submit
                                    </Button>
                                </Control>
                            </Field>
                        </form>
                    </Content>
                </Container>
            </Section>
        </Layout>
    );
};

BookDetailPage.getInitialProps = async ({ query }: NextPageContext) => {
    const { id } = query;
    return { bookID: Array.isArray(id) ? id[0] : id };
};

export default BookDetailPage;
