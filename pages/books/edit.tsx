import React, { FormEvent, useState, useEffect } from 'react';
import { NextPage } from 'next';
import { useDispatch } from 'react-redux';
import { Section, Container, Button, Field, Label, Control } from 'bloomer';
import { Layout } from '../../components/Layout';
import { Book, updateBook, addBook } from '../../modules/BookModule';
import { httpClient } from '../../rest/HttpClient';

interface Props {
    book?: Book;
}

const BooksEditPage: NextPage<Props> = ({ book }) => {
    const now = new Date();
    const [bookID, setBookID] = useState('');
    const [title, setTitle] = useState('');
    const [authors, setAuthors] = useState('');
    const [datePublished, setDatePublished] = useState('');
    const [description, setDescription] = useState('');
    const [coverImage, setCoverImage] = useState('');
    const [userID, setUserID] = useState('');
    const [timestamp, setTimestamp] = useState(now);
    const [updatedAt, setUpdatedAt] = useState(now);
    const [isExistBook, setIsExistBook] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        if (book) {
            setBookID(book.bookID);
            setTitle(book.title);
            setAuthors(book.authors.join(','));
            setDatePublished(book.datePublished);
            setDescription(book.description);
            setCoverImage(book.coverImage);
            setIsExistBook(true);
            setUserID(book.userID);
            setTimestamp(book.timestamp);
            setUpdatedAt(book.updatedAt);
        }
    }, [book]);

    const clearStates = () => {
        const now = new Date();
        setTitle('');
        setAuthors('');
        setDatePublished('');
        setDescription('');
        setCoverImage('');
        setUserID('');
        setTimestamp(now);
        setUpdatedAt(now);
    };

    const handleFormSubmit = async (e: FormEvent) => {
        e.preventDefault();

        if (!title || !authors || !datePublished || !description || !coverImage) {
            return;
        }

        if (isExistBook) {
            await dispatch(
                updateBook({
                    bookID,
                    title,
                    authors: authors.split(','),
                    datePublished,
                    description,
                    coverImage,
                    userID,
                    timestamp,
                    updatedAt,
                }),
            );
        } else {
            await dispatch(
                addBook({
                    title,
                    authors,
                    datePublished,
                    description,
                    coverImage,
                    userID,
                    timestamp,
                    updatedAt,
                }),
            );
            clearStates();
        }
    };

    return (
        <Layout title="add books">
            <Section>
                <Container className="edit__container">
                    <form onSubmit={handleFormSubmit}>
                        <Field>
                            <Label>Title</Label>
                            <Control>
                                <input
                                    className="input"
                                    type="text"
                                    value={title}
                                    onChange={e => {
                                        setTitle(e.target.value);
                                    }}
                                />
                            </Control>
                        </Field>

                        <Field>
                            <Label>Authors</Label>
                            <Control>
                                <input
                                    className="input"
                                    type="text"
                                    placeholder="AAA,BBB,CCC"
                                    value={authors}
                                    onChange={e => {
                                        setAuthors(e.target.value);
                                    }}
                                />
                            </Control>
                        </Field>

                        <Field>
                            <Label>Date Published</Label>
                            <Control>
                                <input
                                    className="input"
                                    type="text"
                                    placeholder="2019/08/01"
                                    value={datePublished}
                                    onChange={e => {
                                        setDatePublished(e.target.value);
                                    }}
                                />
                            </Control>
                        </Field>

                        <Field>
                            <Label>Description</Label>
                            <Control>
                                <textarea
                                    className="textarea"
                                    value={description}
                                    onChange={e => {
                                        setDescription(e.target.value);
                                    }}
                                />
                            </Control>
                        </Field>

                        <Field>
                            <Control>
                                <Label>Cover Image</Label>
                                <input
                                    className="input"
                                    value={coverImage}
                                    onChange={e => {
                                        setCoverImage(e.target.value);
                                    }}
                                />
                                {coverImage && (
                                    <figure className="preview">
                                        <img src={coverImage} />
                                    </figure>
                                )}
                            </Control>
                        </Field>

                        <Field>
                            <Button type="submit" isColor="link">
                                {isExistBook ? 'Edit' : 'Add'}
                            </Button>
                        </Field>
                    </form>
                </Container>
            </Section>

            <style jsx>{`
                .preview {
                    max-width: 200px;
                }
            `}</style>
        </Layout>
    );
};

BooksEditPage.getInitialProps = async ({ query }) => {
    console.log('query', query);
    if (query.id) {
        const { id } = query;
        const book = await httpClient.books.getBook(Array.isArray(id) ? id[0] : id);
        return { book: book.bookID === '' ? undefined : book };
    }
    return { book: undefined };
};

export default BooksEditPage;
