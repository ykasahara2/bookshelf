import * as RestTypes from '../RestTypes';
import * as UserModule from '../../modules/UserModule';

export function parseUsers(restUsers: RestTypes.User[]): UserModule.User[] {
    return restUsers.map(user => parseUser(user));
}

export function parseUser(restUser: RestTypes.User): UserModule.User {
    return {
        userID: restUser.id,
        nickname: restUser.nickname,
        mailAddr: restUser.mail_addr,
        updatedAt: restUser.updated_at,
    };
}
