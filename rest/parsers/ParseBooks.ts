import * as RestTypes from '../RestTypes';
import * as BookModule from '../../modules/BookModule';

export function parseBooks(restBooks: RestTypes.Book[]): BookModule.Book[] {
    return restBooks.map(book => parseBook(book));
}

export function parseBook(restBook: RestTypes.Book): BookModule.Book {
    return {
        bookID: restBook.id,
        title: restBook.title,
        authors: restBook.authors,
        datePublished: restBook.date_published,
        description: restBook.description,
        coverImage: restBook.cover_image,
        userID: restBook.user_id,
        timestamp: restBook.timestamp,
        updatedAt: restBook.updated_at,
    };
}
