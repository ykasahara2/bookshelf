import * as RestTypes from '../RestTypes';
import * as BookReviewModule from '../../modules/BookReviewModule';

export function parseBookReviews(restBookReviews: RestTypes.BookReview[]): BookReviewModule.BookReview[] {
    return restBookReviews.map(br => parseBookReview(br));
}

export function parseBookReview(restBookReview: RestTypes.BookReview): BookReviewModule.BookReview {
    return {
        reviewID: restBookReview.review_id,
        bookID: restBookReview.book_id,
        name: restBookReview.name,
        comment: restBookReview.comment,
        postDate: restBookReview.post_date,
    };
}
