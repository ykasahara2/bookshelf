import { AxiosInstance } from 'axios';

import * as RestTypes from './RestTypes';
import { parseBooks, parseBook } from './parsers/ParseBooks';
import * as BookModule from '../modules/BookModule';

export class HttpBookClient {
    constructor(private axios: AxiosInstance) {}

    public async getAllBooks(): Promise<BookModule.Book[]> {
        const res = await this.axios.get('/books');
        return parseBooks(res.data);
    }

    public async getBook(bookID: string): Promise<BookModule.Book> {
        const res = await this.axios.get(`/books/${bookID}`);
        return parseBook(res.data);
    }

    public async postBook({
        title,
        authors,
        datePublished,
        description,
        coverImage,
        userID,
        timestamp,
        updatedAt,
    }: BookModule.NewBook): Promise<string> {
        const body: RestTypes.PostBook = {
            title: title,
            authors: authors.split(','),
            date_published: datePublished,
            description: description,
            cover_image: coverImage,
            user_id: userID,
            timestamp: timestamp,
            updated_at: updatedAt,
        };
        console.log('body', body);
        const res = await this.axios.post(`/books`, body);
        return res.data;
    }

    public async putBook({
        bookID,
        title,
        authors,
        datePublished,
        description,
        coverImage,
        userID,
        timestamp,
        updatedAt,
    }: BookModule.Book): Promise<void | Error> {
        const body: RestTypes.Book = {
            id: bookID,
            title: title,
            authors: authors,
            date_published: datePublished,
            description: description,
            cover_image: coverImage,
            user_id: userID,
            timestamp: timestamp,
            updated_at: updatedAt,
        };
        const res = await this.axios.put(`/books`, body);
        return res.data.result;
    }

    public async deleteBook(bookID: string) {
        const res = await this.axios.delete(`/books/${bookID}`);
        return res.data;
    }
}
