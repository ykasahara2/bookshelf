import axios, { AxiosInstance } from 'axios';

import { HttpBookClient } from './HttpBookClient';
import { HttpBookReviewClient } from './HttpBookReviewClient';
import { HttpUserClient } from './HttpUserClient';

export class HttpClient {
    constructor(private axios: AxiosInstance) {}

    public async init() {
        return await this.axios.get('/');
    }

    public get books(): HttpBookClient {
        return new HttpBookClient(this.axios);
    }

    public get bookReviews(): HttpBookReviewClient {
        return new HttpBookReviewClient(this.axios);
    }

    public get users(): HttpUserClient {
        return new HttpUserClient(this.axios);
    }
}

export interface ApiResult {
    status: number;
    data: { results: any };
}

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
export const httpClient = new HttpClient(
    axios.create({
        baseURL: 'http://localhost:9000/api',
    }),
);

// function getBlob(blobURL: string): Promise<Blob> {
//     return new Promise<Blob>((resolve, error) => {
//         const xhr = new XMLHttpRequest();
//         xhr.onload = () => {
//             resolve(xhr.response);
//         };
//         xhr.responseType = 'blob';
//         xhr.open('GET', blobURL);
//         xhr.send();
//     });
// }

// function wait(): Promise<void> {
//     return new Promise(resolve => {
//         setTimeout(() => resolve());
//     });
// }
