import { AxiosInstance } from 'axios';

import * as RestTypes from './RestTypes';
import * as BookReviewModule from '../modules/BookReviewModule';
import { parseBookReviews, parseBookReview } from './parsers/ParseBookReviews';

export class HttpBookReviewClient {
    constructor(private axios: AxiosInstance) {}

    public async getAllBookReviews(): Promise<BookReviewModule.BookReview[]> {
        const res = await this.axios.get('/book-reviews');
        return parseBookReviews(res.data);
    }

    public async getBookReview(reviewID: string): Promise<BookReviewModule.BookReview> {
        const res = await this.axios.get(`/book-reviews/${reviewID}`);
        return parseBookReview(res.data);
    }

    public async postBookReview({ userID, bookID, name, comment }: BookReviewModule.NewBookReview): Promise<string> {
        const body: RestTypes.PostBookReview = {
            user_id: userID,
            book_id: bookID,
            name: name,
            comment: comment,
        };
        const res = await this.axios.post(`/book-reviews`, body);
        return res.data;
    }
}
