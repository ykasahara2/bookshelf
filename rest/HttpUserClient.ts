import { AxiosInstance } from 'axios';

import * as RestTypes from './RestTypes';
import { parseUsers, parseUser } from './parsers/ParseUsers';
import * as UserModule from '../modules/UserModule';

export class HttpUserClient {
    constructor(private axios: AxiosInstance) {}

    public async getAllUsers(): Promise<UserModule.User[]> {
        const res = await this.axios.get('/users');
        return parseUsers(res.data.results);
    }

    public async getUser(userID: number): Promise<UserModule.User> {
        const res = await this.axios.get(`/users/${userID}`);
        return parseUser(res.data.results);
    }

    public async postUser({ nickname, mailAddr, updatedAt }: UserModule.PostUser): Promise<string> {
        const body: RestTypes.PostUser = {
            nickname,
            mail_addr: mailAddr,
            updated_at: updatedAt,
        };
        const res = await this.axios.post(`/users`, body);
        return res.data.results;
    }

    public async putUser({ userID, nickname, mailAddr, updatedAt }: UserModule.User): Promise<void | Error> {
        const body: RestTypes.User = {
            id: userID,
            nickname,
            mail_addr: mailAddr,
            updated_at: updatedAt,
        };
        const res = await this.axios.put(`/users`, body);
        return res.data.result;
    }

    public async removeUser(userID: number) {
        const res = await this.axios.delete(`/users/${userID}`);
        return res.data.results;
    }
}
