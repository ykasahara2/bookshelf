export interface Book {
    id: string;
    title: string;
    authors: string[];
    date_published: string;
    description: string;
    cover_image: string;
    user_id: string;
    timestamp: Date;
    updated_at: Date;
}

export interface PostBook extends Omit<Book, 'id'> {}

export interface User {
    id: string;
    nickname: string;
    mail_addr: string;
    updated_at: Date;
}

export interface PostUser extends Omit<User, 'id'> {}

export interface BookReview {
    review_id: string;
    book_id: string;
    name: string;
    comment: string;
    post_date: string;
}

export interface PostBookReview extends Omit<BookReview, 'review_id' | 'post_date'> {
    user_id: string;
}
