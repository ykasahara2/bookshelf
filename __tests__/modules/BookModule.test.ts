import { BookModule, Book, initialState } from '../../modules/BookModule';

const defaultUpdatedAt = new Date('2018/08/01');
const defaultTimeStamp = new Date('2018/08/01');

const testBooks: Book[] = [
    {
        bookID: 'book1',
        title: '実践Rust入門[言語仕様から開発手法まで]',
        authors: ['κeen', '河野 達也', '小松礼人'],
        datePublished: '2019/05/08',
        description: `Rustは2015年に安定版がリリースされた新しい言語です。静的型付けと関数型言語などにみられる高度な抽象化のしくみを取り入れており，高品質で再利用性の高いプログラムを開発できます。さらに，ハードウェア資源についてC/C++と同等の効率の良い制御ができますが，決定的に違うのは，安全性をかなり重視しています。つまりRustは開発者の生産性を高めつつ，性能やハードウェア資源の効率を最大限に発揮するという，従来の言語では相反する要件を同時に満たす，数少ないプログラミング言語の1つなのです。本書はこの注目のプログラミング言語Rustの入門書です。この1冊でRustの言語仕様から開発現場で必要となる知識までを丁寧に解説しています。
        `,
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51e5B1Zx%2ByL._SX352_BO1,204,203,200_.jpg',
        userID: 'user1',
        timestamp: defaultTimeStamp,
        updatedAt: defaultUpdatedAt,
    },
    {
        bookID: 'book2',
        title: 'Go言語でつくるインタプリタ',
        authors: ['Thorsten Ball'],
        datePublished: '2018/06/16',
        description: `本書は、Go言語でプログラミング言語のインタプリタを作りながら、プログラミング言語とそのインタプリタについて学ぶ書籍です。
        順を追ってコードを示し、C言語風の構文を持つ言語「Monkeyプログラミング言語」のインタプリタを組み立てていきます。字句解析器、構文解析器、評価器を作りながら、ソースコードをトークン列に、トークン列を抽象構文木に変換し、その抽象構文木を評価し実行する方法を学びます。さらに、インタプリタに新しいデータ型を導入し、組み込み関数を追加して、言語を拡張していきます。付録では構文マクロシステムについても扱います。
        本書では、Go言語標準のツールキット以外のサードパーティライブラリやフレームワークは使用せず、0行のコードからはじめて、完動するインタプリタができあがるところまでを体験します。その過程を通じて、プログラミング言語とインタプリタの仕組みを実践的に学ぶことができます。`,
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51sLCPa8DBL._SX258_BO1,204,203,200_.jpg',
        userID: 'user1',
        timestamp: defaultTimeStamp,
        updatedAt: defaultUpdatedAt,
    },
    {
        bookID: 'book3',
        title: 'コンピュータシステムの理論と実装 ―モダンなコンピュータの作り方',
        authors: ['Noam Nisan', 'Shimon Schocken'],
        datePublished: '2015/03/25',
        description: `コンピュータを理解するための最善の方法はゼロからコンピュータを作ることです。コンピュータの構成要素は、ハードウェア、ソフトウェア、コンパイラ、OSに大別できます。本書では、これらコンピュータの構成要素をひとつずつ組み立てます。具体的には、NANDという電子素子からスタートし、論理ゲート、加算器、CPUを設計します。そして、オペレーティングシステム、コンパイラ、バーチャルマシンなどを実装しコンピュータを完成させて、最後にその上でアプリケーション（テトリスなど）を動作させます。実行環境はJava（Mac、Windows、Linuxで動作）。
        `,
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/514ifs4Y5bL._SX352_BO1,204,203,200_.jpg',
        userID: 'user2',
        timestamp: defaultTimeStamp,
        updatedAt: defaultUpdatedAt,
    },
];

describe('createSlice', () => {
    it('setBooks', () => {
        const { actions, reducer } = BookModule;
        const books = reducer(initialState, actions.setBooks(testBooks)).books;
        expect(books.length).toEqual(testBooks.length);
        expect(books[0].title).toEqual(testBooks[0].title);
    });

    it('addBook', () => {
        const { actions, reducer } = BookModule;
        const newBook: Book = {
            bookID: '',
            title: 'title',
            authors: ['author'],
            datePublished: '2019/08/01',
            description: 'description',
            coverImage: 'http://curucuru.co.jp/',
            userID: 'user1',
            timestamp: defaultTimeStamp,
            updatedAt: defaultUpdatedAt,
        };
        const books = reducer(initialState, actions.addBook(newBook)).books;
        expect(books[books.length - 1]).toEqual(newBook);
    });

    it('updateBook', () => {
        const { actions, reducer } = BookModule;
        const newBook: Book = {
            bookID: 'book1',
            title: 'title',
            authors: ['author'],
            datePublished: '2019/08/01',
            description: 'description',
            coverImage: 'http://curucuru.co.jp/',
            userID: 'user1',
            timestamp: defaultTimeStamp,
            updatedAt: defaultUpdatedAt,
        };
        initialState.books = testBooks;
        const books = reducer(initialState, actions.updateBook(newBook)).books;
        expect(books[0]).toEqual(newBook);
    });

    it('removeBook', () => {
        const { actions, reducer } = BookModule;
        initialState.books = testBooks;
        const books = reducer(initialState, actions.removeBook('book1')).books;
        expect(books.length).toEqual(testBooks.length - 1);
        expect(books[0].title).toEqual(testBooks[1].title);
    });
});
