import { UserModule, User, initialState } from '../../modules/UserModule';

const defaultUpdatedAt = new Date('2018/08/01');

const testUsers: User[] = [
    {
        userID: '1',
        nickname: 'スティーブ・ジョブズ',
        mailAddr: 'stevejobs@example.com',
        updatedAt: defaultUpdatedAt,
    },
    {
        userID: '2',
        nickname: 'ウォルト・ディズニー',
        mailAddr: 'waltdisnery@exaple.com',
        updatedAt: defaultUpdatedAt,
    },
    {
        userID: '3',
        nickname: '孫正義',
        mailAddr: 'masayoshison@example.com',
        updatedAt: defaultUpdatedAt,
    },
];

const testUser = {
    userID: '-1',
    nickname: 'nickname',
    mailAddr: 'aaabbbccc@example.com',
    updatedAt: defaultUpdatedAt,
};

describe('createSlice', () => {
    it('setUsers', () => {
        const { actions, reducer } = UserModule;
        const users = reducer(initialState, actions.setUsers(testUsers)).users;
        expect(users.length).toEqual(testUsers.length);
        expect(users[0].nickname).toEqual(testUsers[0].nickname);
    });

    it('addUser', () => {
        const { actions, reducer } = UserModule;
        const newUser: User = testUser;
        const users = reducer(initialState, actions.addUser(newUser)).users;
        expect(users[users.length - 1]).toEqual(newUser);
    });

    it('updateUser', () => {
        const { actions, reducer } = UserModule;
        const newUser: User = { ...testUser, userID: '1' };
        initialState.users = testUsers;
        const users = reducer(initialState, actions.updateUser(newUser)).users;
        expect(users[0]).toEqual(newUser);
    });

    it('removeUser', () => {
        const { actions, reducer } = UserModule;
        initialState.users = testUsers;
        const users = reducer(initialState, actions.removeUser('1')).users;
        expect(users.length).toEqual(testUsers.length - 1);
        expect(users[0].nickname).toEqual(testUsers[1].nickname);
    });
});
