import { Optional } from 'typescript-optional';
import { Book } from '../modules/BookModule';
import { BookReview } from '../modules/BookReviewModule';

export class BookModels extends Array<BookModel> {
    public getByID(bookID: string): Optional<BookModel> {
        return Optional.ofNullable(this.find(book => book.bookID === bookID));
    }
}

class BookModel {
    constructor(
        public bookID: string,
        public title: string,
        public authors: string[],
        public datePublished: string,
        public description: string,
        public coverImage: string,
        public userID: string,
        public timestamp: Date,
        public updatedAt: Date,
        public bookReviews: BookReview[],
    ) {}
}

export function modelingBooks(books: Book[], bookReviews: BookReview[]): BookModels {
    return new BookModels(
        ...books.map(book => {
            const matchBookReviews = bookReviews.filter(br => br.bookID === book.bookID);
            return new BookModel(
                book.bookID,
                book.title,
                book.authors,
                book.datePublished,
                book.description,
                book.coverImage,
                book.userID,
                book.timestamp,
                book.updatedAt,
                matchBookReviews,
            );
        }),
    );
}
