import { Optional } from 'typescript-optional';
import { User } from '../modules/UserModule';

export class UserModels extends Array<User> {
    public getByID(userID: string): Optional<User> {
        return Optional.ofNullable(this.find(user => user.userID === userID));
    }

    public getNickname(userID: string): Optional<string> {
        return Optional.ofNullable(this.find(user => user.userID === userID)).map(user => user.nickname);
    }
}

export function modelingUsers(allUsers: User[]): UserModels {
    return new UserModels(...allUsers);
}
