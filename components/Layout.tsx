import React from 'react';
import Head from 'next/head';
import { Footer, Hero, HeroHeader, Nav, Navbar, NavbarBrand } from 'bloomer';
import Link from 'next/link';

interface Props {
    title: string;
}

export const Layout: React.FunctionComponent<Props> = ({ children, title }) => {
    return (
        <div>
            <Head>
                <title>{title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <header>
                <Hero isColor="link">
                    <HeroHeader>
                        <Nav>
                            <Navbar>
                                <NavbarBrand>
                                    <Link href="/">
                                        <a className="navbar-item">
                                            <p className="title">Bookshelf</p>
                                        </a>
                                    </Link>
                                    <Link href="/books">
                                        <a className="navbar-item">books</a>
                                    </Link>
                                    {/* <Link href="/login">
                                        <a className="navbar-item">login</a>
                                    </Link>
                                    <Link href="/signup">
                                        <a className="navbar-item">signup</a>
                                    </Link> */}
                                </NavbarBrand>
                            </Navbar>
                        </Nav>
                    </HeroHeader>
                </Hero>
            </header>
            {children}
            <Footer>
                <p className="has-text-centered">©bookshelf 2019</p>
            </Footer>
        </div>
    );
};
