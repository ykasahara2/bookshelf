import * as React from 'react';
import { Columns, Column } from 'bloomer';
import { Book } from '../modules/BookModule';

type BookDetailProps = {
    book: Book;
};

export const BookDetail: React.FunctionComponent<BookDetailProps> = ({ book }) => (
    <div>
        <Columns>
            <Column isSize="1/3">
                <img src={book.coverImage} />
            </Column>
            <Column>
                <div className="content">
                    <div>
                        <span className="has-text-weight-bold is-size-5">{book.title}</span>
                        {` `}
                        <span>-</span>
                        {` `}
                        <span>{book.datePublished}</span>
                    </div>
                    <div className="author-container subtitle is-size-6">
                        {book.authors.map((author, index) => (
                            <div key={index}>
                                <span className="author">{author}</span>
                            </div>
                        ))}
                        <div>(著)</div>
                    </div>
                    <div className="BookDetail__description">{book.description}</div>
                </div>
            </Column>
        </Columns>

        <style jsx>{`
            .author-container {
                display: flex;
            }

            .author {
                padding: 0 0.25rem;
            }

            .BookDetail__description {
                white-space: pre-wrap;
            }
        `}</style>
    </div>
);
