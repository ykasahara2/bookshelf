import * as React from 'react';
import { BookListItem } from './BookListItem';
import { Book } from '../modules/BookModule';

type Props = {
    books: Book[];
};

export const BookList: React.FunctionComponent<Props> = ({ books }) => {
    if (!books || books.length === 0) {
        return <div />;
    }

    return (
        <ul>
            {books.map(book => (
                <li key={book.bookID} className="BookList__item">
                    <BookListItem book={book} />
                </li>
            ))}

            <style jsx>{`
                .BookList__item:not(:last-child) {
                    margin-bottom: 1.5rem;
                }
            `}</style>
        </ul>
    );
};
