import * as React from 'react';
import Link from 'next/link';

import { Book } from '../modules/BookModule';

type Props = {
    book: Book;
};

export const BookListItem: React.FunctionComponent<Props> = ({ book }) => (
    <div>
        <Link href={`books/detail?id=${book.bookID}`}>
            <a>
                <p>{book.title}</p>
                <figure className="BookListItem__figure">
                    <img className="BookListItem__img" src={book.coverImage} />
                </figure>
            </a>
        </Link>
    </div>
);
