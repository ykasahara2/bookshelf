# bookshelf
このプロジェクトは Next.js / redux-starter-kit / gae のサンプルです。

## redux-starter-kit の参考資料

* https://redux-starter-kit.js.org
* https://qiita.com/uehaj/items/f91d68ea4ef8450fc45c
* https://qiita.com/pullphone/items/fdb0f36d8b4e5c0ae893

## テスト

next サーバの起動

```
$ yarn dev
```

gae サーバの起動

```
$ cd server
$ ./serve.sh
```