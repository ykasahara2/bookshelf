package main

import (
	"crypto/rand"
	"encoding/binary"
	"strconv"
	"time"
)

type Service interface {
	RandomString() string
	Now() time.Time
}

type ServiceImpl struct{}

func (s ServiceImpl) RandomString() string {
	var n uint64
	binary.Read(rand.Reader, binary.LittleEndian, &n)
	return strconv.FormatUint(n, 36)
}

func (s ServiceImpl) Now() time.Time {
	return time.Now()
}

type ServiceMock struct {
	mockRandom string
	mockTime   time.Time
}

func (s ServiceMock) RandomString() string {
	return s.mockRandom
}

func (s ServiceMock) Now() time.Time {
	return s.mockTime
}
