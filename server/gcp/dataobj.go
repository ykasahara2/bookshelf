package gcp

import "time"

type DataObjects struct {
	CacheBooks  []*CacheBook
	Users       []*User
	BookReviews []*BookReview
}

type CacheBook struct {
	BookID          string `datastore:"-" goon:"id"`
	Book            Book   `datastore:"Book,noindex"`
	BookReviewCount int    `datastore:"BookReviewCount"`
}

type Book struct {
	BookID        string    `datastore:"-" goon:"id"`
	Title         string    `datastore:"Title"`
	Authors       []string  `datastore:"Authors"`
	DatePublished string    `datastore:"DatePublished"`
	Description   string    `datastore:"Description"`
	CoverImage    string    `datastore:"CoverImage"`
	UserID        string    `datastore:"UserID"`
	Timestamp     time.Time `datastore:"Timestamp"`
	UpdatedAt     time.Time `datastore:"UpdatedAt"`
}

type User struct {
	UserID    string    `datastore:"-" goon:"id"`
	Nickname  string    `datastore:"Nickname,noindex"`
	MailAddr  string    `datastore:"MailAddr"`
	UpdatedAt time.Time `datastore:"UpdatedAt"`
}

type UserPassword struct {
	UserID    string    `datastore:"-" goon:"id"`
	Password  string    `datastore:"Password"`
	UpdatedAt time.Time `datastore:"UpdatedAt"`
}

type UserPassword2 struct {
	UserID    string    `datastore:"-" goon:"id"`
	Password  string    `datastore:"Password"`
	UpdatedAt time.Time `datastore:"UpdatedAt"`
}

type BookReview struct {
	ReviewID string `datastore:"-" goon:"id"`
	BookID   string `datastore:"BookID"`
	Name     string `datastore:"Name"`
	Comment  string `datastore:"Comment"`
	PostDate string `datestore:"PostDate"`
}
