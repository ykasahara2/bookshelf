package gcp

import (
	"net/http"
	"net/url"

	"github.com/mjibson/goon"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/taskqueue"
)

type CacheBookOperate struct {
	BookID string
}

func GetCacheBooks(r *http.Request) []*CacheBook {
	var cacheBooks []*CacheBook
	q := datastore.NewQuery("CacheBook")
	g := goon.NewGoon(r)
	g.GetAll(q, &cacheBooks)
	return cacheBooks
}

func GetCacheBookKeys(r *http.Request) ([]*datastore.Key, error) {
	var cacheBooks []*CacheBook
	q := datastore.NewQuery("CacheBook")
	g := goon.NewGoon(r)
	return g.GetAll(q, &cacheBooks)
}

func GetCacheBook(r *http.Request, bookID string) (*CacheBook, error) {
	cacheBook := &CacheBook{BookID: bookID}
	g := goon.NewGoon(r)
	if err := g.Get(cacheBook); err != nil {
		return nil, err
	}
	return cacheBook, nil
}

func GetPopularCacheBooks(r *http.Request, limit int) []*CacheBook {
	var cacheBooks []*CacheBook
	q := datastore.NewQuery("CacheBook").Order("-BookReviewCount").Limit(limit)
	g := goon.NewGoon(r)
	g.GetAll(q, &cacheBooks)
	return cacheBooks
}

func PutCacheBook(r *http.Request, cacheBook CacheBook) {
	g := goon.NewGoon(r)
	g.Put(&cacheBook)
}

func DeleteCacheBooks(r *http.Request) error {
	keys, err := GetCacheBookKeys(r)
	if err != nil {
		return err
	}

	g := goon.NewGoon(r)
	err = g.DeleteMulti(keys)
	if err != nil {
		return err
	}

	return nil
}

func DeleteCacheBook(r *http.Request, bookID string) error {
	cacheBook, err := GetCacheBook(r, bookID)
	if err != nil {
		return err
	}

	g := goon.NewGoon(r)
	if err := g.Delete(cacheBook); err != nil {
		return err
	}
	return nil
}

func GetBooks(r *http.Request) []*Book {
	var books []*Book
	q := datastore.NewQuery("Book")
	g := goon.NewGoon(r)
	g.GetAll(q, &books)
	return books
}

func GetBook(r *http.Request, bookID string) (*Book, error) {
	book := &Book{BookID: bookID}
	g := goon.NewGoon(r)
	if err := g.Get(book); err != nil {
		return nil, err
	}
	return book, nil
}

func PutBook(r *http.Request, book Book) {
	g := goon.NewGoon(r)
	g.Put(&book)
}

func DeleteBook(r *http.Request, bookID string) error {
	book := &Book{BookID: bookID}
	g := goon.NewGoon(r)
	if err := g.Delete(book); err != nil {
		return err
	}
	return nil
}

func GetUsers(r *http.Request) []*User {
	var users []*User
	q := datastore.NewQuery("User")
	g := goon.NewGoon(r)
	g.GetAll(q, &users)
	return users
}

func GetBookReviews(r *http.Request) []*BookReview {
	var bookReviews []*BookReview
	q := datastore.NewQuery("BookReview")
	g := goon.NewGoon(r)
	g.GetAll(q, &bookReviews)
	return bookReviews
}

func GetBookReviewsByBookID(r *http.Request, bookID string) []*BookReview {
	var bookReviews []*BookReview
	q := datastore.NewQuery("BookReview").Filter("BookID=", bookID)
	g := goon.NewGoon(r)
	g.GetAll(q, &bookReviews)
	return bookReviews
}

func GetBookReview(r *http.Request, reviewID string) (*BookReview, error) {
	bookReview := &BookReview{ReviewID: reviewID}
	g := goon.NewGoon(r)
	if err := g.Get(bookReview); err != nil {
		return nil, err
	}
	return bookReview, nil
}

func PutBookReview(r *http.Request, bookReview BookReview) {
	g := goon.NewGoon(r)
	g.Put(&bookReview)
}

func WriteAll(r *http.Request, writeObjects []interface{}) {
	for _, o := range writeObjects {
		switch o := o.(type) {
		case Book:
			PutBook(r, o)
		case BookReview:
			PutBookReview(r, o)
		case CacheBook:
			PutCacheBook(r, o)
		}
	}
}

func CacheOperations(r *http.Request, operations []interface{}) {
	ctx := appengine.NewContext(r)

	for _, o := range operations {
		switch o := o.(type) {
		case CacheBookOperate:
			parameters := url.Values{}
			t := taskqueue.NewPOSTTask("/cache/books/"+o.BookID, parameters)
			taskqueue.Add(ctx, t, "")
		}
	}
}
