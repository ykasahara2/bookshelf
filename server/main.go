package main

import (
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/mjibson/goon"
	"google.golang.org/appengine"
	"google.golang.org/appengine/taskqueue"

	"bookshelf/server/gcp"
)

type Response struct {
	StatusCode      int
	ResponseBody    interface{}
	WriteObjects    []interface{}
	CacheOperations []interface{}
}

type GetPopularBooksParam struct {
	Limit int
}

type PostBookParam struct {
	RequestBody PostBookJson
}

type PostBookReviewParam struct {
	RequestBody PostBookReviewJson
}

func main() {
	gin.SetMode(gin.DebugMode)
	router := gin.New()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "X-Requested-With", "Content-Range", "Content-Disposition", "Content-Type", "Authorization"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	http.Handle("/", handleRouter(router))

	appengine.Main()
}

func handleRouter(router *gin.Engine) *gin.Engine {
	router.GET("/api/", func(c *gin.Context) {
		// DataObject の初期化とキャッシュの作成
		DataObjectsInit(c.Request)
		ctx := appengine.NewContext(c.Request)
		parameters := url.Values{}
		t := taskqueue.NewPOSTTask("/cache/books", parameters)
		taskqueue.Add(ctx, t, "")

		c.String(http.StatusOK, "HELLO")
	})

	router.GET("/api/books", func(c *gin.Context) {
		sort := c.Query("sort")
		limit := c.Query("limit")
		if sort == "book_review_count_desc" {
			limit_num, err := strconv.Atoi(limit)
			if err != nil {
				c.JSON(http.StatusBadRequest, "limit の指定が正しくありません")
				return
			}

			inquiries := []Inquiry{
				InquiryGetPopularBooks{
					Limit: limit_num,
				},
			}
			dataObjects := Fetch(c.Request, inquiries)
			res := GetPopularBooks(GetPopularBooksParam{Limit: limit_num}, d2m(dataObjects))

			c.JSON(res.StatusCode, res.ResponseBody)
		} else {
			books := gcp.GetBooks(c.Request)
			c.JSON(http.StatusOK, assignBooks(books))
		}
	})

	router.GET("/api/books/:id", func(c *gin.Context) {
		bookID := c.Param("id")
		book, err := gcp.GetBook(c.Request, bookID)
		if err != nil {
			c.JSON(http.StatusNotFound, err.Error())
			return
		}
		c.JSON(http.StatusOK, book2json(book))
	})

	router.POST("/api/books", func(c *gin.Context) {
		var bookBody PostBookJson
		c.BindJSON(&bookBody)

		inquiries := []Inquiry{}
		dataObjects := Fetch(c.Request, inquiries)
		res := PostBook(PostBookParam{bookBody}, d2m(dataObjects), ServiceImpl{})

		gcp.WriteAll(c.Request, res.WriteObjects)
		c.JSON(res.StatusCode, res.ResponseBody)
	})

	router.PUT("/api/books", func(c *gin.Context) {
		var bookBody BookJson
		c.BindJSON(&bookBody)

		newBook := gcp.Book{
			BookID:        bookBody.ID,
			Title:         bookBody.Title,
			Authors:       bookBody.Authors,
			DatePublished: bookBody.DatePublished,
			Description:   bookBody.Description,
			CoverImage:    bookBody.CoverImage,
			UserID:        bookBody.UserID,
			Timestamp:     bookBody.Timestamp,
			UpdatedAt:     bookBody.UpdatedAt,
		}

		gcp.PutBook(c.Request, newBook)
		c.JSON(http.StatusCreated, newBook.BookID)
	})

	router.DELETE("/api/books/:id", func(c *gin.Context) {
		id := c.Param("id")
		err := gcp.DeleteBook(c.Request, id)

		if err != nil {
			c.JSON(http.StatusNotFound, err.Error())
			return
		}

		c.JSON(http.StatusOK, "deleted")
	})

	router.GET("/api/users", func(c *gin.Context) {
		users := gcp.GetUsers(c.Request)
		c.JSON(http.StatusOK, assignUsers(users))
	})

	router.GET("/api/book-reviews", func(c *gin.Context) {
		bookReviews := gcp.GetBookReviews(c.Request)
		c.JSON(http.StatusOK, assignBookViews(bookReviews))
	})

	router.POST("/api/book-reviews", func(c *gin.Context) {
		var bookReviewBody PostBookReviewJson
		c.BindJSON(&bookReviewBody)

		inquiries := []Inquiry{
			InquiryGetCacheBook{BookID: bookReviewBody.BookID},
		}
		dataObjects := Fetch(c.Request, inquiries)
		res := PostBookReview(PostBookReviewParam{RequestBody: bookReviewBody}, d2m(dataObjects), ServiceImpl{})

		gcp.WriteAll(c.Request, res.WriteObjects)
		gcp.CacheOperations(c.Request, res.CacheOperations)
		c.JSON(res.StatusCode, res.ResponseBody)
	})

	router.POST("/cache/books", func(c *gin.Context) {
		gcp.DeleteCacheBooks(c.Request)

		books := gcp.GetBooks(c.Request)
		bookReviews := gcp.GetBookReviews(c.Request)
		res := PostCacheBooks(books, bookReviews)

		gcp.WriteAll(c.Request, res.WriteObjects)
		c.JSON(res.StatusCode, res.ResponseBody)
	})

	router.POST("/cache/books/:id", func(c *gin.Context) {
		bookID := c.Param("id")
		gcp.DeleteCacheBook(c.Request, bookID)

		book, err := gcp.GetBook(c.Request, bookID)
		if err != nil {
			c.JSON(http.StatusNotFound, err.Error())
			return
		}

		bookReviews := gcp.GetBookReviewsByBookID(c.Request, bookID)
		res := PostCacheBook(book, len(bookReviews))

		gcp.WriteAll(c.Request, res.WriteObjects)
		c.JSON(res.StatusCode, res.ResponseBody)
	})

	return router
}

func assignBookModels(bookModels BookModels) []BookJson {
	results := []BookJson{}
	for _, bm := range bookModels {
		results = append(results, book2json(&gcp.Book{
			BookID:        bm.ID,
			Title:         bm.Title,
			Authors:       bm.Authors,
			DatePublished: bm.DatePublished,
			Description:   bm.Description,
			CoverImage:    bm.CoverImage,
			UserID:        bm.UserID,
			Timestamp:     bm.Timestamp,
			UpdatedAt:     bm.UpdatedAt,
		}))
	}
	return results
}

func assignBooks(books []*gcp.Book) []BookJson {
	results := []BookJson{}
	for _, book := range books {
		results = append(results, book2json(book))
	}
	return results
}

func book2json(book *gcp.Book) BookJson {
	return BookJson{
		ID:            book.BookID,
		Title:         book.Title,
		Authors:       book.Authors,
		DatePublished: book.DatePublished,
		Description:   book.Description,
		CoverImage:    book.CoverImage,
		UserID:        book.UserID,
		Timestamp:     book.Timestamp,
		UpdatedAt:     book.UpdatedAt,
	}
}

func assignUsers(users []*gcp.User) []UserJson {
	results := []UserJson{}
	for _, user := range users {
		results = append(results, UserJson{
			ID:        user.UserID,
			Nickname:  user.Nickname,
			MailAddr:  user.MailAddr,
			UpdatedAt: user.UpdatedAt,
		})
	}
	return results
}

func assignBookViews(bookUserReviews []*gcp.BookReview) []BookReviewJson {
	results := []BookReviewJson{}
	for _, bookUserReview := range bookUserReviews {
		results = append(results, BookReviewJson{
			ID:       bookUserReview.ReviewID,
			BookID:   bookUserReview.BookID,
			Name:     bookUserReview.Name,
			Comment:  bookUserReview.Comment,
			PostDate: bookUserReview.PostDate,
		})
	}
	return results
}

func GetPopularBooks(p GetPopularBooksParam, models Models) Response {
	return Response{
		StatusCode:   http.StatusOK,
		ResponseBody: assignBookModels(models.BookModels.Take(p.Limit)),
	}
}

func PostBook(p PostBookParam, models Models, s Service) Response {
	newBook := gcp.Book{
		BookID:        s.RandomString(),
		Title:         p.RequestBody.Title,
		Authors:       p.RequestBody.Authors,
		DatePublished: p.RequestBody.DatePublished,
		Description:   p.RequestBody.Description,
		CoverImage:    p.RequestBody.CoverImage,
		UserID:        p.RequestBody.UserID,
		Timestamp:     s.Now(),
		UpdatedAt:     s.Now(),
	}

	if models.BookModels.isExistBookByTitle(newBook.Title) {
		return Response{
			StatusCode: http.StatusConflict,
		}
	}

	return Response{
		StatusCode:   http.StatusCreated,
		ResponseBody: newBook.BookID,
		WriteObjects: []interface{}{newBook},
	}
}

func PostBookReview(p PostBookReviewParam, models Models, s Service) Response {
	newBookReview := gcp.BookReview{
		ReviewID: s.RandomString(),
		BookID:   p.RequestBody.BookID,
		Name:     p.RequestBody.Name,
		Comment:  p.RequestBody.Comment,
		PostDate: s.Now().Format("2006/01/02 15:04"),
	}

	if !models.BookModels.isExistBook(newBookReview.BookID) {
		return Response{
			StatusCode: http.StatusNotFound,
		}
	}

	cacheOperations := gcp.CacheBookOperate{
		BookID: newBookReview.BookID,
	}

	return Response{
		StatusCode:      http.StatusCreated,
		ResponseBody:    newBookReview.ReviewID,
		WriteObjects:    []interface{}{newBookReview},
		CacheOperations: []interface{}{cacheOperations},
	}
}

func PostCacheBooks(books []*gcp.Book, bookReviews []*gcp.BookReview) Response {
	var writeObjects []interface{}

	for _, b := range books {
		writeObjects = append(writeObjects, gcp.CacheBook{
			BookID:          b.BookID,
			Book:            *b,
			BookReviewCount: countBookReviews(b.BookID, bookReviews),
		})
	}

	return Response{
		StatusCode:   http.StatusOK,
		WriteObjects: writeObjects,
	}
}

func PostCacheBook(book *gcp.Book, bookReviewCount int) Response {
	newCacheBook := gcp.CacheBook{
		BookID:          book.BookID,
		Book:            *book,
		BookReviewCount: bookReviewCount,
	}
	return Response{
		StatusCode:   http.StatusOK,
		WriteObjects: []interface{}{newCacheBook},
	}
}

func countBookReviews(bookID string, bookReviews []*gcp.BookReview) int {
	result := 0
	for _, br := range bookReviews {
		if br.BookID == bookID {
			result += 1
		}
	}
	return result
}

func DataObjectsInit(r *http.Request) {
	defaultTime := time.Date(2019, 8, 1, 0, 0, 0, 0, time.Local)

	g := goon.NewGoon(r)
	g.PutMulti([]*gcp.Book{
		{
			BookID:        "book1",
			Title:         "実践Rust入門[言語仕様から開発手法まで]",
			Authors:       []string{"κeen", "河野 達也", "小松礼人"},
			DatePublished: "2019/05/08",
			Description:   `Rustは2015年に安定版がリリースされた新しい言語です。静的型付けと関数型言語などにみられる高度な抽象化のしくみを取り入れており，高品質で再利用性の高いプログラムを開発できます。さらに，ハードウェア資源についてC/C++と同等の効率の良い制御ができますが，決定的に違うのは，安全性をかなり重視しています。つまりRustは開発者の生産性を高めつつ，性能やハードウェア資源の効率を最大限に発揮するという，従来の言語では相反する要件を同時に満たす，数少ないプログラミング言語の1つなのです。本書はこの注目のプログラミング言語Rustの入門書です。この1冊でRustの言語仕様から開発現場で必要となる知識までを丁寧に解説しています。`,
			CoverImage:    "https://images-na.ssl-images-amazon.com/images/I/51e5B1Zx%2ByL._SX352_BO1,204,203,200_.jpg",
			UserID:        "user1",
			Timestamp:     defaultTime,
			UpdatedAt:     defaultTime,
		},
		{
			BookID:        "book2",
			Title:         "Go言語でつくるインタプリタ",
			Authors:       []string{"Thorsten Ball"},
			DatePublished: "2018/06/16",
			Description: `本書は、Go言語でプログラミング言語のインタプリタを作りながら、プログラミング言語とそのインタプリタについて学ぶ書籍です。
        順を追ってコードを示し、C言語風の構文を持つ言語「Monkeyプログラミング言語」のインタプリタを組み立てていきます。字句解析器、構文解析器、評価器を作りながら、ソースコードをトークン列に、トークン列を抽象構文木に変換し、その抽象構文木を評価し実行する方法を学びます。さらに、インタプリタに新しいデータ型を導入し、組み込み関数を追加して、言語を拡張していきます。付録では構文マクロシステムについても扱います。
        本書では、Go言語標準のツールキット以外のサードパーティライブラリやフレームワークは使用せず、0行のコードからはじめて、完動するインタプリタができあがるところまでを体験します。その過程を通じて、プログラミング言語とインタプリタの仕組みを実践的に学ぶことができます。`,
			CoverImage: "https://images-na.ssl-images-amazon.com/images/I/51sLCPa8DBL._SX258_BO1,204,203,200_.jpg",
			UserID:     "user1",
			Timestamp:  defaultTime,
			UpdatedAt:  defaultTime,
		},
		{
			BookID:        "book3",
			Title:         "コンピュータシステムの理論と実装 ―モダンなコンピュータの作り方",
			Authors:       []string{"Noam Nisan", "Shimon Schocken"},
			DatePublished: "2015/03/25",
			Description:   `コンピュータを理解するための最善の方法はゼロからコンピュータを作ることです。コンピュータの構成要素は、ハードウェア、ソフトウェア、コンパイラ、OSに大別できます。本書では、これらコンピュータの構成要素をひとつずつ組み立てます。具体的には、NANDという電子素子からスタートし、論理ゲート、加算器、CPUを設計します。そして、オペレーティングシステム、コンパイラ、バーチャルマシンなどを実装しコンピュータを完成させて、最後にその上でアプリケーション（テトリスなど）を動作させます。実行環境はJava（Mac、Windows、Linuxで動作）。`,
			CoverImage:    "https://images-na.ssl-images-amazon.com/images/I/514ifs4Y5bL._SX352_BO1,204,203,200_.jpg",
			UserID:        "user2",
			Timestamp:     defaultTime,
			UpdatedAt:     defaultTime,
		},
	})

	g.PutMulti([]*gcp.User{
		{UserID: "user1", Nickname: "aaa", MailAddr: "aaa@example.com", UpdatedAt: defaultTime},
		{UserID: "user2", Nickname: "bbb", MailAddr: "bbb@example.com", UpdatedAt: defaultTime},
		{UserID: "user3", Nickname: "ccc", MailAddr: "ccc@example.com", UpdatedAt: defaultTime},
	})

	g.PutMulti([]*gcp.BookReview{
		{ReviewID: "review1", BookID: "book1", Name: "ジョージ", Comment: "おもしろい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
		{ReviewID: "review2", BookID: "book1", Name: "王", Comment: "かなしい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
		{ReviewID: "review3", BookID: "book2", Name: "田中太郎", Comment: "たのしい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
	})
}
