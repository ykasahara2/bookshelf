package main

import (
	"bookshelf/server/gcp"
	"net/http"
	"testing"
	"time"
)

var serviceMockBook = ServiceMock{
	mockRandom: "new_book_id",
	mockTime:   time.Date(2019, time.April, 1, 10, 0, 0, 0, time.UTC),
}
var serviceMockBookReview = ServiceMock{
	mockRandom: "new_book_review_id",
	mockTime:   time.Date(2019, time.April, 1, 10, 0, 0, 0, time.UTC),
}

var defaultTime = time.Date(2019, 8, 1, 0, 0, 0, 0, time.Local)
var books = []*gcp.Book{
	{
		BookID:        "book1",
		Title:         "実践Rust入門[言語仕様から開発手法まで]",
		Authors:       []string{"κeen", "河野 達也", "小松礼人"},
		DatePublished: "2019/05/08",
		Description:   `Rustは2015年に安定版がリリースされた新しい言語です。静的型付けと関数型言語などにみられる高度な抽象化のしくみを取り入れており，高品質で再利用性の高いプログラムを開発できます。さらに，ハードウェア資源についてC/C++と同等の効率の良い制御ができますが，決定的に違うのは，安全性をかなり重視しています。つまりRustは開発者の生産性を高めつつ，性能やハードウェア資源の効率を最大限に発揮するという，従来の言語では相反する要件を同時に満たす，数少ないプログラミング言語の1つなのです。本書はこの注目のプログラミング言語Rustの入門書です。この1冊でRustの言語仕様から開発現場で必要となる知識までを丁寧に解説しています。`,
		CoverImage:    "https://images-na.ssl-images-amazon.com/images/I/51e5B1Zx%2ByL._SX352_BO1,204,203,200_.jpg",
		UserID:        "user1",
		Timestamp:     defaultTime,
		UpdatedAt:     defaultTime,
	},
	{
		BookID:        "book2",
		Title:         "Go言語でつくるインタプリタ",
		Authors:       []string{"Thorsten Ball"},
		DatePublished: "2018/06/16",
		Description: `本書は、Go言語でプログラミング言語のインタプリタを作りながら、プログラミング言語とそのインタプリタについて学ぶ書籍です。
	順を追ってコードを示し、C言語風の構文を持つ言語「Monkeyプログラミング言語」のインタプリタを組み立てていきます。字句解析器、構文解析器、評価器を作りながら、ソースコードをトークン列に、トークン列を抽象構文木に変換し、その抽象構文木を評価し実行する方法を学びます。さらに、インタプリタに新しいデータ型を導入し、組み込み関数を追加して、言語を拡張していきます。付録では構文マクロシステムについても扱います。
	本書では、Go言語標準のツールキット以外のサードパーティライブラリやフレームワークは使用せず、0行のコードからはじめて、完動するインタプリタができあがるところまでを体験します。その過程を通じて、プログラミング言語とインタプリタの仕組みを実践的に学ぶことができます。`,
		CoverImage: "https://images-na.ssl-images-amazon.com/images/I/51sLCPa8DBL._SX258_BO1,204,203,200_.jpg",
		UserID:     "user1",
		Timestamp:  defaultTime,
		UpdatedAt:  defaultTime,
	},
	{
		BookID:        "book3",
		Title:         "コンピュータシステムの理論と実装 ―モダンなコンピュータの作り方",
		Authors:       []string{"Noam Nisan", "Shimon Schocken"},
		DatePublished: "2015/03/25",
		Description:   `コンピュータを理解するための最善の方法はゼロからコンピュータを作ることです。コンピュータの構成要素は、ハードウェア、ソフトウェア、コンパイラ、OSに大別できます。本書では、これらコンピュータの構成要素をひとつずつ組み立てます。具体的には、NANDという電子素子からスタートし、論理ゲート、加算器、CPUを設計します。そして、オペレーティングシステム、コンパイラ、バーチャルマシンなどを実装しコンピュータを完成させて、最後にその上でアプリケーション（テトリスなど）を動作させます。実行環境はJava（Mac、Windows、Linuxで動作）。`,
		CoverImage:    "https://images-na.ssl-images-amazon.com/images/I/514ifs4Y5bL._SX352_BO1,204,203,200_.jpg",
		UserID:        "user2",
		Timestamp:     defaultTime,
		UpdatedAt:     defaultTime,
	},
}
var bookReviews = []*gcp.BookReview{
	{ReviewID: "review1", BookID: "book1", Name: "ジョージ", Comment: "おもしろい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
	{ReviewID: "review2", BookID: "book1", Name: "王", Comment: "かなしい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
	{ReviewID: "review3", BookID: "book2", Name: "田中太郎", Comment: "たのしい本です", PostDate: defaultTime.Format("2006/01/02 15:04:05")},
}
var users = []*gcp.User{
	{UserID: "user1", Nickname: "aaa", MailAddr: "aaa@example.com", UpdatedAt: defaultTime},
	{UserID: "user2", Nickname: "bbb", MailAddr: "bbb@example.com", UpdatedAt: defaultTime},
	{UserID: "user3", Nickname: "ccc", MailAddr: "ccc@example.com", UpdatedAt: defaultTime},
}
var cacheBooks = []*gcp.CacheBook{
	{BookID: books[0].BookID, Book: *books[0], BookReviewCount: 2},
	{BookID: books[1].BookID, Book: *books[1], BookReviewCount: 1},
	{BookID: books[2].BookID, Book: *books[2], BookReviewCount: 0},
}

func Test_PostBook_BookConflict(t *testing.T) {
	p := PostBookParam{
		RequestBody: PostBookJson{
			Title:         books[0].Title,
			Authors:       books[0].Authors,
			DatePublished: books[0].DatePublished,
			Description:   books[0].Description,
			CoverImage:    books[0].CoverImage,
			UserID:        books[0].UserID,
			Timestamp:     books[0].Timestamp,
			UpdatedAt:     books[0].UpdatedAt,
		},
	}
	res := PostBook(p, d2m(gcp.DataObjects{CacheBooks: cacheBooks, BookReviews: bookReviews, Users: users}), serviceMockBook)

	if res.StatusCode != http.StatusConflict {
		t.Fatalf("該当の本が既に存在するのにのに409にならない. %d", res.StatusCode)
	}
}

func Test_PostBook_Created(t *testing.T) {
	p := PostBookParam{
		RequestBody: PostBookJson{
			Title:         books[0].Title + "2",
			Authors:       books[0].Authors,
			DatePublished: books[0].DatePublished,
			Description:   books[0].Description,
			CoverImage:    books[0].CoverImage,
			UserID:        books[0].UserID,
			Timestamp:     books[0].Timestamp,
			UpdatedAt:     books[0].UpdatedAt,
		},
	}
	res := PostBook(p, d2m(gcp.DataObjects{CacheBooks: cacheBooks, BookReviews: bookReviews, Users: users}), serviceMockBook)
	writeObject := res.WriteObjects[0].(gcp.Book)

	if writeObject.BookID != serviceMockBook.mockRandom {
		t.Fatalf("BookIDが違う. %s", writeObject.BookID)
	}
	if writeObject.Title != books[0].Title+"2" {
		t.Fatalf("タイトルが違う. %s", writeObject.Title)
	}
	if writeObject.Timestamp != serviceMockBook.mockTime {
		t.Fatalf("タイムスタンプが違う. %s", writeObject.Timestamp)
	}
}

func Test_PostBookReview_BookNotFound(t *testing.T) {
	p := PostBookReviewParam{
		RequestBody: PostBookReviewJson{
			BookID:   "book999",
			Name:     "hoge",
			Comment:  "comment",
			PostDate: "2019/10/01",
		},
	}
	res := PostBookReview(p, d2m(gcp.DataObjects{CacheBooks: cacheBooks, BookReviews: bookReviews, Users: users}), serviceMockBookReview)

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("該当の本がないのに404にならない. %d", res.StatusCode)
	}
}

func Test_PostBookReview_Created(t *testing.T) {
	p := PostBookReviewParam{
		RequestBody: PostBookReviewJson{
			BookID:   "book1",
			Name:     "hoge",
			Comment:  "comment",
			PostDate: "2019/10/01",
		},
	}
	res := PostBookReview(p, d2m(gcp.DataObjects{CacheBooks: cacheBooks, BookReviews: bookReviews, Users: users}), serviceMockBookReview)
	writeObject := res.WriteObjects[0].(gcp.BookReview)

	if writeObject.ReviewID != serviceMockBookReview.mockRandom {
		t.Fatalf("ReviewIDが違う. %s", writeObject.ReviewID)
	}
	if writeObject.Comment != "comment" {
		t.Fatalf("コメントが違う. %s", writeObject.Comment)
	}
	if writeObject.PostDate != "2019/04/01 10:00" {
		t.Fatalf("投稿日が違う. %s", writeObject.PostDate)
	}
}
