module bookshelf/server

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/mjibson/goon v1.0.0
	google.golang.org/appengine v1.6.2
)
