package main

import (
	"bookshelf/server/gcp"
	"net/http"
	"reflect"
)

type (
	Inquiry interface {
		fetch(r *http.Request) []interface{}
	}

	InquiryGetCacheBook struct {
		BookID string
	}

	InquiryGetPopularBooks struct {
		Limit int
	}

	InquiryGetBookReview struct {
		ReviewID string
	}

	InquiryGetUser struct {
		UserID string
	}
)

func (inquiry InquiryGetCacheBook) fetch(r *http.Request) []interface{} {
	var results []interface{}
	book, err := gcp.GetCacheBook(r, inquiry.BookID)
	if err == nil {
		results = append(results, book)
	}
	return results
}

func (inquiry InquiryGetPopularBooks) fetch(r *http.Request) []interface{} {
	var results []interface{}
	cacheBooks := gcp.GetPopularCacheBooks(r, inquiry.Limit)
	for _, cb := range cacheBooks {
		results = append(results, cb)
	}
	return results
}

func (inquiry InquiryGetBookReview) fetch(r *http.Request) []interface{} {
	var results []interface{}
	bookReview, err := gcp.GetBookReview(r, inquiry.ReviewID)
	if err == nil {
		results = append(results, bookReview)
	}
	return results
}

func Fetch(r *http.Request, inquiries []Inquiry) gcp.DataObjects {
	var ret gcp.DataObjects
	for _, i := range inquiries {
		ret = appendEach(ret, i.fetch(r))
	}
	return ret
}

func appendEach(objects gcp.DataObjects, list []interface{}) gcp.DataObjects {
	for _, i := range list {
		hpv := reflect.ValueOf(&objects)
		t := hpv.Elem().Type()
		n := t.NumField()
		for fieldIndex := 0; fieldIndex < n; fieldIndex++ {
			field := t.Field(fieldIndex)
			fieldType := field.Type
			if fieldType.Kind() != reflect.Slice {
				continue
			}
			match := fieldType.Elem().Elem() == reflect.TypeOf(i).Elem()
			if !match {
				continue
			}
			l := reflect.Append(hpv.Elem().Field(fieldIndex), reflect.ValueOf(i))
			hpv.Elem().Field(fieldIndex).Set(l)
		}
	}
	return objects
}
