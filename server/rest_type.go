package main

import "time"

type (
	BookJson struct {
		ID            string    `json:"id"`
		Title         string    `json:"title"`
		Authors       []string  `json:"authors"`
		DatePublished string    `json:"date_published"`
		Description   string    `json:"description"`
		CoverImage    string    `json:"cover_image"`
		UserID        string    `json:"user_id"`
		Timestamp     time.Time `json:"timestamp"`
		UpdatedAt     time.Time `json:"updated_at"`
	}

	UserJson struct {
		ID        string    `json:"id"`
		Nickname  string    `json:"nickname"`
		MailAddr  string    `json:"mail_addr"`
		UpdatedAt time.Time `json:"updated_at"`
	}

	BookReviewJson struct {
		ID       string `json:"review_id"`
		BookID   string `json:"book_id"`
		Name     string `json:"name"`
		Comment  string `json:"comment"`
		PostDate string `json:"post_date"`
	}

	PostBookJson struct {
		Title         string    `json:"title"`
		Authors       []string  `json:"authors"`
		DatePublished string    `json:"date_published"`
		Description   string    `json:"description"`
		CoverImage    string    `json:"cover_image"`
		UserID        string    `json:"user_id"`
		Timestamp     time.Time `json:"timestamp"`
		UpdatedAt     time.Time `json:"updated_at"`
	}

	PostBookReviewJson struct {
		BookID   string `json:"book_id"`
		Name     string `json:"name"`
		Comment  string `json:"comment"`
		PostDate string `json:"post_date"`
	}
)
