package main

import (
	"bookshelf/server/gcp"
	"testing"
)

func Test_ObjectAppends(t *testing.T) {
	d := gcp.DataObjects{}
	list := []interface{}{
		books[0], books[1], books[2],
		bookReviews[0], bookReviews[1], bookReviews[2],
		users[0], users[1], users[2],
	}
	actual := appendEach(d, list)

	if len(actual.Users) != 3 {
		t.Fatalf("ユーザーが3人ではなく%d", len(actual.Users))
	}

	if actual.Users[0].UserID != "user1" {
		t.Fatalf("actual.Users[0].IDがuser1ではなく%s", actual.Users[0].UserID)
	}
}
