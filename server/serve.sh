#!/bin/bash

export GIN_MODE=debug
if [ -z "$VAR" ]; then
    APP_PORT=9000
fi
ADMIN_PORT=`expr $APP_PORT + 1`

dev_appserver.py --enable_host_checking=false --clear_datastore --host=0.0.0.0 --port=$APP_PORT --admin_host=0.0.0.0 --admin_port=$ADMIN_PORT --datastore_consistency_policy=consistent app.yaml
