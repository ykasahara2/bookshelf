package main

import (
	"bookshelf/server/gcp"
	"time"
)

type Models struct {
	BookModels       BookModels
	BookReviewModels BookReviewModels
	UserModels       UserModels
}

type BookModels []BookModel

type BookModel struct {
	ID            string
	Title         string
	Authors       []string
	DatePublished string
	Description   string
	CoverImage    string
	UserID        string
	Timestamp     time.Time
	UpdatedAt     time.Time
}

func (bookModels BookModels) isExistBook(bookID string) bool {
	for _, bm := range bookModels {
		if bm.ID == bookID {
			return true
		}
	}
	return false
}

func (bookModels BookModels) isExistBookByTitle(title string) bool {
	for _, bm := range bookModels {
		if bm.Title == title {
			return true
		}
	}
	return false
}

func (bookModels BookModels) Take(limit int) BookModels {
	if len(bookModels) > limit {
		return bookModels[:limit]
	}
	return bookModels
}

type BookReviewModels []BookReviewModel

type BookReviewModel struct {
	ID       string
	BookID   string
	Name     string
	Comment  string
	PostDate string
}

func (bookReviewModels BookReviewModels) isExistBookReview(bookReviewID string) bool {
	for _, brm := range bookReviewModels {
		if brm.ID == bookReviewID {
			return true
		}
	}
	return false
}

type UserModels []UserModel

type UserModel struct {
	ID        string
	Nickname  string
	MailAddr  string
	UpdatedAt time.Time
}

func d2m(dataObjects gcp.DataObjects) Models {
	return Models{
		BookModels:       dcb2bm(dataObjects.CacheBooks),
		BookReviewModels: dbr2brm(dataObjects.BookReviews),
		UserModels:       du2um(dataObjects.Users),
	}
}

func dcb2bm(cacheBooks []*gcp.CacheBook) BookModels {
	var ret BookModels
	for _, cb := range cacheBooks {
		ret = append(ret, BookModel{
			ID:            cb.BookID,
			Title:         cb.Book.Title,
			Authors:       cb.Book.Authors,
			DatePublished: cb.Book.DatePublished,
			Description:   cb.Book.Description,
			CoverImage:    cb.Book.CoverImage,
			UserID:        cb.Book.UserID,
			Timestamp:     cb.Book.Timestamp,
			UpdatedAt:     cb.Book.UpdatedAt,
		})
	}
	return ret
}

func dbr2brm(bookReviews []*gcp.BookReview) BookReviewModels {
	var ret BookReviewModels
	for _, bookReview := range bookReviews {
		ret = append(ret, BookReviewModel{
			ID:       bookReview.ReviewID,
			BookID:   bookReview.BookID,
			Name:     bookReview.Name,
			Comment:  bookReview.Comment,
			PostDate: bookReview.PostDate,
		})
	}
	return ret
}

func du2um(users []*gcp.User) UserModels {
	var ret UserModels
	for _, user := range users {
		ret = append(ret, UserModel{
			ID:        user.UserID,
			Nickname:  user.Nickname,
			MailAddr:  user.MailAddr,
			UpdatedAt: user.UpdatedAt,
		})
	}
	return ret
}
