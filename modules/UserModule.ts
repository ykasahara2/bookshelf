import { createSlice, PayloadAction } from 'redux-starter-kit';

export interface User {
    userID: string;
    nickname: string;
    mailAddr: string;
    updatedAt: Date;
}

export interface PostUser extends Omit<User, 'userID'> {}

export interface UserState {
    users: User[];
}

export const initialState: UserState = {
    users: [
        {
            userID: 'user1',
            nickname: 'スティーブ・ジョブズ',
            mailAddr: 'stevejobs@example.com',
            updatedAt: new Date('2019/08/01'),
        },
        {
            userID: 'user2',
            nickname: 'ウォルト・ディズニー',
            mailAddr: 'waltdisnery@exaple.com',
            updatedAt: new Date('2019/08/01'),
        },
        {
            userID: 'user3',
            nickname: '孫正義',
            mailAddr: 'masayoshison@example.com',
            updatedAt: new Date('2019/08/01'),
        },
    ],
};

export const UserModule = createSlice({
    initialState,
    reducers: {
        setUsers: (state: UserState, action: PayloadAction<User[]>) => {
            state.users = action.payload;
        },
        addUser: (state: UserState, action: PayloadAction<User>) => {
            state.users.push(action.payload);
        },
        updateUser: (state: UserState, action: PayloadAction<User>) => {
            const index = state.users.findIndex(user => user.userID === action.payload.userID);
            state.users[index] = action.payload;
        },
        removeUser: (state: UserState, action: PayloadAction<string>) => {
            state.users = state.users.filter(user => user.userID !== action.payload);
        },
    },
});
