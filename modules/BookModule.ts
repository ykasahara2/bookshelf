import { createSlice, PayloadAction } from 'redux-starter-kit';
import { httpClient } from '../rest/HttpClient';
import { AppThunk, AppDispatch } from '../store';
import { RootState } from '../reducer';
import { Services } from '../services';

export interface Book {
    bookID: string;
    title: string;
    authors: string[];
    datePublished: string;
    description: string;
    coverImage: string;
    userID: string;
    timestamp: Date;
    updatedAt: Date;
}

export interface BookState {
    books: Book[];
}

export interface NewBook extends Omit<Book, 'bookID' | 'authors'> {
    authors: string;
}

export const initialState: BookState = {
    books: [],
};

export const BookModule = createSlice({
    initialState,
    reducers: {
        setBooks: (state: BookState, action: PayloadAction<Book[]>) => {
            state.books = action.payload;
        },
        addBook: (state: BookState, action: PayloadAction<Book>) => {
            state.books.push(action.payload);
        },
        updateBook: (state: BookState, action: PayloadAction<Book>) => {
            const index = state.books.findIndex(book => book.bookID === action.payload.bookID);
            state.books[index] = action.payload;
        },
        removeBook: (state: BookState, action: PayloadAction<string>) => {
            state.books = state.books.filter(book => book.bookID !== action.payload);
        },
    },
});

export const fetchBooks = (): AppThunk => async dispatch => {
    const books = await httpClient.books.getAllBooks();
    dispatch(BookModule.actions.setBooks(books));
};

export const addBook = (newBook: NewBook): AppThunk => {
    return async (dispatch: AppDispatch, _getState: () => RootState, extraArgument: Services) => {
        const bookID = await extraArgument.httpClient.books.postBook(newBook);
        if (!bookID) {
            return;
        }

        dispatch(
            BookModule.actions.addBook({
                ...newBook,
                bookID: bookID,
                authors: newBook.authors.split(','),
            }),
        );
    };
};

export const updateBook = (book: Book): AppThunk => {
    return async (dispatch: AppDispatch, _getState: () => RootState, extraArgument: Services) => {
        await extraArgument.httpClient.books.putBook(book);
        dispatch(BookModule.actions.updateBook(book));
    };
};

export const deleteBook = (bookID: string): AppThunk => {
    return async (dispatch: AppDispatch, _getState: () => RootState, extraArgument: Services) => {
        await extraArgument.httpClient.books.deleteBook(bookID);
        dispatch(BookModule.actions.removeBook(bookID));
    };
};
