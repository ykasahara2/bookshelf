import { createSlice, PayloadAction } from 'redux-starter-kit';
import { AppThunk, AppDispatch } from '../store';
import { Services } from '../services';
import { httpClient } from '../rest/HttpClient';
import { RootState } from '../reducer';

export interface BookReview {
    reviewID: string;
    bookID: string;
    name: string;
    comment: string;
    postDate: string;
}

export interface BookReviewState {
    bookReviews: BookReview[];
}

export interface NewBookReview extends Omit<BookReview, 'reviewID' | 'postDate'> {
    userID: string;
}

export const initialState: BookReviewState = {
    bookReviews: [],
};

export const BookReviewModule = createSlice({
    initialState,
    reducers: {
        setBookReviews: (state: BookReviewState, action: PayloadAction<BookReview[]>) => {
            state.bookReviews = action.payload;
        },
        addBookReview: (state: BookReviewState, action: PayloadAction<BookReview>) => {
            state.bookReviews.push(action.payload);
        },
        updateBookReview: (state: BookReviewState, action: PayloadAction<BookReview>) => {
            const index = state.bookReviews.findIndex(bookReview => bookReview.reviewID === action.payload.reviewID);
            state.bookReviews[index] = action.payload;
        },
        removeBookReview: (state: BookReviewState, action: PayloadAction<string>) => {
            state.bookReviews = state.bookReviews.filter(bookReview => bookReview.reviewID !== action.payload);
        },
    },
});

export const fetchBookReviews = (): AppThunk => async dispatch => {
    const bookReviews = await httpClient.bookReviews.getAllBookReviews();
    dispatch(BookReviewModule.actions.setBookReviews(bookReviews));
};

export const postBookReview = (userID: string, bookID: string, name: string, comment: string): AppThunk => {
    return async (dispatch: AppDispatch, getState: () => RootState, extraArgument: Services) => {
        const dummyReviewID = '#DUMMY_REVIEW_ID#';
        const postDate = extraArgument.dateTimeService.getNowTimestamp();
        const newBookReview: NewBookReview = { userID, bookID, name, comment };

        // 楽観的 UI
        dispatch(
            BookReviewModule.actions.addBookReview({
                ...newBookReview,
                reviewID: dummyReviewID,
                postDate,
            }),
        );

        const reviewID = await extraArgument.httpClient.bookReviews.postBookReview(newBookReview);
        const newBookReviews: BookReview[] = [
            ...getState().bookReview.bookReviews.filter(br => br.reviewID !== dummyReviewID),
            { ...newBookReview, reviewID, postDate },
        ];
        dispatch(BookReviewModule.actions.setBookReviews(newBookReviews));
    };
};
