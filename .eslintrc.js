module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
        jest: true,
    },
    extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['import', 'prettier', 'react', 'react-hooks'],
    rules: {
        'prettier/prettier': [
            'error',
            {
                printWidth: 120,
                tabWidth: 4,
                useTabs: false,
                singleQuote: true,
                proseWrap: 'preserve',
                trailingComma: 'all',
                maxChaining: 1,
            },
        ],
        '@typescript-eslint/camelcase': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/no-empty-interface': [
            'error',
            {
                allowSingleExtends: true,
            },
        ],
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-unused-vars': [
            'off',
            {
                argsIgnorePattern: '^_',
            },
        ],
        '@typescript-eslint/no-use-before-define': 'off',
        'react/jsx-no-target-blank': 'error',
        'react/prop-types': 'off',
        'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
        'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
        // ordered-imports - tslint
        'import/order': [
            'error',
            {
                groups: [['external', 'builtin'], ['internal', 'index', 'sibling', 'parent']],
            },
        ],
        'no-console': 'warn',
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
};
