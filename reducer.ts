import { combineReducers } from 'redux-starter-kit';
import { BookModule } from './modules/BookModule';
import { BookReviewModule } from './modules/BookReviewModule';

export const rootReducer = combineReducers({
    book: BookModule.reducer,
    bookReview: BookReviewModule.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
