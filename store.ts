import { Action, configureStore, getDefaultMiddleware } from 'redux-starter-kit';
import { ThunkAction } from 'redux-thunk';
import logger from 'redux-logger';
import { rootReducer, RootState } from './reducer';
import { Services, DateTimeService } from './services';
import { httpClient } from './rest/HttpClient';

export const store = (() => {
    const extraArgument: Services = {
        httpClient: httpClient,
        dateTimeService: new DateTimeService(),
    };
    const customizedMiddleware = getDefaultMiddleware({
        thunk: {
            extraArgument,
        },
        serializableCheck: false,
    });
    const middlewares = [...customizedMiddleware];

    if (process.env.NODE_ENV === 'development') {
        middlewares.push(logger);
    }

    return configureStore({
        reducer: rootReducer,
        middleware: middlewares,
    });
})();

export type AppDispatch = typeof store.dispatch;

export type AppThunk = ThunkAction<void, RootState, Services, Action<string>>;
